# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 14:17:47 2019

@author: cxx
"""
import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *

#%%
params={}

params['imgDir']='./img-new/figure1'
params['imgFile'] = './img-new/figure1.svg'
params['ImgDir']='\\img-new\\figure1\\'
params['datDir']='/burg_100/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/'
params['curDir']="D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper"
params['start'] = 2000
params['polar'] = 'Polar.00002000.dat'
params['stress'] = 'Stress.00002001.dat'
params['flexo'] = 'Eflexo.00002001.dat'

params['nx']=512
params['ny']=1
params['nz']=512

params['xrange']=[244,268]
params['yrange']=[114,138]

params['disLocX']=0.5
params['disLocY']=0.5

params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']

touch(params['imgDir'])
x1=params['xrange'][0]
x2=params['xrange'][1]
y1=params['yrange'][0]
y2=params['yrange'][1]

params['captionFile'] = './img-new/figure1.md'
params['caption'] = """

## Figure 1
![Figure 1](figure1.svg)

Stress and strain gradient distribution around dislocation core. 
(a) stress11, (b) stress33, (c) stress13, 
(d) strain 11 gradient, (e) strain 33 gradient, (f) strain 13 gradient.

"""

#%% open the file, important always remember to close it
#dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')
# be careful with the gradient, since we store the data in x,y,z order, but when plotting, the 2D array is mapped directly to the image, 
# the x and y axis needs to be reversed, so the gradient we calculated using the original data needs to be switched when used.
dataAll= h5py.File('20190422_STO_dislocation_flexo_mech.h5','r')
# a s11
s11 = np.array(dataAll[params['datDir']+'stress11'][x1:x2,y1:y2])
# b s33
s33 = np.array(dataAll[params['datDir']+'stress33'][x1:x2,y1:y2])
# c s13
s13 = np.array(dataAll[params['datDir']+'stress13'][x1:x2,y1:y2])

density = 1

# a s11
str11 = np.array(dataAll[params['datDir']+'elasticStrain11'][x1:x2,y1:y2])
# b s33
str33 = np.array(dataAll[params['datDir']+'elasticStrain33'][x1:x2,y1:y2])
# c s13
str13 = np.array(dataAll[params['datDir']+'elasticStrain13'][x1:x2,y1:y2])

e11gY,e11gX = np.gradient(str11,edge_order=1)
e11gLength = np.sqrt(np.square(np.array(e11gX))+np.square(np.array(e11gY)))

e33gY,e33gX = np.gradient(str33,edge_order=1)
e33gLength = np.sqrt(np.square(np.array(e33gX))+np.square(np.array(e33gY)))

e13gY,e13gX = np.gradient(str13,edge_order=1)
e13gLength = np.sqrt(np.square(np.array(e13gX))+np.square(np.array(e13gY)))

# a s11
s11gX = dataAll[params['datDir']+'stress11GradientX'][x1:x2:density,y1:y2:density]
s11gY = dataAll[params['datDir']+'stress11GradientY'][x1:x2:density,y1:y2:density]
s11gLength = np.sqrt(np.square(np.array(s11gX))+np.square(np.array(s11gY)))

# b s33
s33gX = dataAll[params['datDir']+'stress33GradientX'][x1:x2:density,y1:y2:density]
s33gY = dataAll[params['datDir']+'stress33GradientY'][x1:x2:density,y1:y2:density]
s33gLength = np.sqrt(np.square(np.array(s33gX))+np.square(np.array(s33gY)))
# c s13
s13gX = dataAll[params['datDir']+'stress13GradientX'][x1:x2:density,y1:y2:density]
s13gY = dataAll[params['datDir']+'stress13GradientY'][x1:x2:density,y1:y2:density]
s13gLength = np.sqrt(np.square(np.array(s13gX))+np.square(np.array(s13gY)))

dataAll.close()

stressRange=[min(s11.min(),s33.min(),s13.min()),max(s11.max(),s33.max(),s13.max())]
gradientStressRange = [min(s11gLength.min(),s33gLength.min(),s13gLength.min()),max(s11gLength.max(),s33gLength.max(),s13gLength.max())]
gradientStrainRange = [min(e11gLength.min(),e33gLength.min(),e13gLength.min()),max(e11gLength.max(),e33gLength.max(),e13gLength.max())]



#%% sub figure a,b,c the stress 11, 33, 13, range from [-5e10,5e10]
cmapGlob = 'inferno'


patch = getScaleBar(4,1,5,1.5,'2nm',color='white',lw=5,fs=30,padding=[1,1],bg = 'None')
heatPlot_noEdge(s11,stressRange,5,params['imgDir']+'/figure_1a_stress11.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,artists=patch)
heatPlot_noEdge(s33,stressRange,5,params['imgDir']+'/figure_1b_stress33.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
heatPlot_noEdge(s13,stressRange,5,params['imgDir']+'/figure_1c_stress13.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
plotColorBar(params['imgDir']+'/figure_1-stressColorBar.svg','Stress (Pa)',[a/1e10 for a in stressRange],4.6,0.7,0.4,1.3,20,'%+0.1f','1e10',cmapGlob=cmapGlob)

#%% sub figure d,e,f the stress gradient, range from []
cmapGlob = 'inferno'


scale = max(s11gLength.max(),s33gLength.max(),s13gLength.max())*15

#scale =1e11
heatQuiverPlot_noEdge(s11gLength,s11gY,s11gX,gradientStressRange,5,scale,params['imgDir']+'/figure_1d_sGrad11.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
heatQuiverPlot_noEdge(s33gLength,s33gY,s33gX,gradientStressRange,5,scale,params['imgDir']+'/figure_1e_sGrad33.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
heatQuiverPlot_noEdge(s13gLength,s13gY,s13gX,gradientStressRange,5,scale,params['imgDir']+'/figure_1f_sGrad13.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
plotColorBar(params['imgDir']+'/figure_1-stressGradientColorBar.svg','Stress Gradient (Pa/nm)',[a/1.0e10 for a in gradientStressRange],4.6,0.7,0.4,1.3,20,'%+.1f','1e10',cmapGlob)


#%%

cmapGlob = 'inferno'


scale = gradientStrainRange[1]*15

#scale =1e11
heatQuiverPlot_noEdge(e11gLength,e11gY,e11gX,gradientStrainRange,5,scale,params['imgDir']+'/figure_1d_eGrad11.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)

times = 4
patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
heatQuiverPlot_noEdge(e33gLength,e33gY,e33gX,gradientStrainRange,5,scale/times,params['imgDir']+'/figure_1e_eGrad33.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,artists=patch)

times = 2
patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
heatQuiverPlot_noEdge(e13gLength,e13gY,e13gX,gradientStrainRange,5,scale/times,params['imgDir']+'/figure_1f_eGrad13.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,artists=patch)
plotColorBar(params['imgDir']+'/figure_1-strainGradientColorBar.svg','Strain Gradient (1/nm)',[a/1e-2 for a in gradientStrainRange],4.6,0.7,0.4,1.3,20,'%+.1f','1e-2',cmapGlob)


#%%
texEquation(r'$\sigma_{11}$',0.8,0.4,params['imgDir']+'/sigma11.svg',20)
texEquation(r'$\sigma_{33}$',0.8,0.4,params['imgDir']+'/sigma33.svg',20)
texEquation(r'$\sigma_{13}$',0.8,0.4,params['imgDir']+'/sigma13.svg',20)
texEquation(r'$\nabla\epsilon_{11}$',0.8,0.4,params['imgDir']+'/nabla_sigma11.svg',20)
texEquation(r'$\nabla\epsilon_{33}$',0.8,0.4,params['imgDir']+'/nabla_sigma33.svg',20)
texEquation(r'$\nabla\epsilon_{13}$',0.8,0.4,params['imgDir']+'/nabla_sigma13.svg',20)
#%%

texEquation(r'a',0.4,0.4,'./img-new/a.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'b',0.4,0.4,'./img-new/b.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'c',0.4,0.4,'./img-new/c.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'd',0.4,0.4,'./img-new/d.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'e',0.4,0.4,'./img-new/e.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)
texEquation(r'f',0.4,0.4,'./img-new/f.svg',20,va='center',ha='center',posX = 0.5,posY=0.5)

trans = True
texEquation(r'a',0.4,0.4,'./img-new/aa.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'b',0.4,0.4,'./img-new/bb.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'c',0.4,0.4,'./img-new/cc.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'd',0.4,0.4,'./img-new/dd.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'e',0.4,0.4,'./img-new/ee.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)
texEquation(r'f',0.4,0.4,'./img-new/ff.svg',20,va='center',ha='center',posX = 0.5,posY=0.5,transparent=trans)


#%% Combine the individual figure files
#figures=[[params['ImgDir']+'figure_1a_stress11.svg',params['ImgDir']+'figure_1a_stress33.svg',params['ImgDir']+'figure_1a_stress13.svg',params['ImgDir']+'figure_1-stressColorBar.svg'],
#         [params['ImgDir']+'figure_1d_sGrad11.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1-stressGradientColorBar.svg']]
#figureName=[['figure_1a_stress11.svg','figure_1b_stress33.svg','figure_1c_stress13.svg','figure_1-stressColorBar.svg'],
#            ['sigma11.svg','sigma33.svg','sigma13.svg',''],
#            ['figure_1d_sGrad11.svg','figure_1e_sGrad33.svg','figure_1f_sGrad13.svg','figure_1-stressGradientColorBar.svg'],
#            ['nabla_sigma11.svg','nabla_sigma33.svg','nabla_sigma13.svg','']]

figureName=[['figure_1a_stress11.svg','figure_1b_stress33.svg','figure_1c_stress13.svg','figure_1-stressColorBar.svg'],
            ['sigma11.svg','sigma33.svg','sigma13.svg',''],
            ['figure_1d_eGrad11.svg','figure_1e_eGrad33.svg','figure_1f_eGrad13.svg','figure_1-strainGradientColorBar.svg'],
            ['nabla_sigma11.svg','nabla_sigma33.svg','nabla_sigma13.svg','']]
figureWidth = 190
figureHeight = 205
textSize = "20"
textBase = 18
textLeft = 5
boxSize="25"
offsetY = 182
figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,4)] for i in range(0,4)]
label = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['a','b','c','d','e','f']]
labelFont = 'DejaVu Sans'
labelWeight = 'normal'
sc.Figure('17cm','11cm',
          sc.Panel(
                  sc.SVG(figures[0][0]).scale(0.5),
                  sc.SVG(label[0]).move(-1,-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][1]).scale(0.5).move(figureWidth,0),
                  sc.SVG(label[1]).move(figureWidth-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][2]).scale(0.5).move(figureWidth*2,0),
                  sc.SVG(label[2]).move(figureWidth*2-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][3]).scale(0.5).move(figureWidth*3,0)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][0]).move(72+figureWidth*0,offsetY)
                  ),          
          sc.Panel(
                  sc.SVG(figures[1][1]).move(72+figureWidth*1,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][2]).move(72+figureWidth*2,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][0]).scale(0.5).move(0,figureHeight),
                  sc.SVG(label[3]).move(0-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][1]).scale(0.5).move(figureWidth,figureHeight),
                  sc.SVG(label[4]).move(figureWidth-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][2]).scale(0.5).move(figureWidth*2,figureHeight),
                  sc.SVG(label[5]).move(figureWidth*2-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][3]).scale(0.5).move(figureWidth*3,figureHeight)
                  ),
          sc.Panel(
                  sc.SVG(figures[3][0]).move(72+figureWidth*0,offsetY+figureHeight)
                  ),          
          sc.Panel(
                  sc.SVG(figures[3][1]).move(72+figureWidth*1,offsetY+figureHeight)
                  ),
          sc.Panel(
                  sc.SVG(figures[3][2]).move(72+figureWidth*2,offsetY+figureHeight)
                  )
          ).save(params['imgFile'])
#          sc.Grid(20,20)
          
#%%
fixSVGASCII(params['imgFile'])
with open(params['captionFile'],'w') as file:
    file.write(params['caption'])