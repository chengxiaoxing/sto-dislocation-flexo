# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 14:17:47 2019

@author: cxx
"""
import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *
#%%
params={}

params['imgDir']='./img-new/figure4'
params['imgFile']='./img-new/figure4.svg'
params['ImgDir']='\\img-new\\figure4\\'
params['datDir']='/burg_100_with_charge_no_flexo/'
params['start'] = 2000
#params['datFold'] = '21+&DISLOCATION1.DEFECTCHARGE_1.0+&PNOISEED_1+QNOISEED_1'
params['datFold'] = '01+&DISLOCATION1.DEFECTCHARGE_0.2+&PNOISEED_1+QNOISEED_1'
params['nx']=512
params['ny']=1
params['nz']=512

params['xrange']=[244,268]
params['yrange']=[114,138]

params['disLocX']=0.5
params['disLocY']=0.5

params['Xrange']=[114,138]
params['Yrange']=[114,138]

params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']

touch(params['imgDir'])

x1=params['xrange'][0]
x2=params['xrange'][1]
y1=params['yrange'][0]
y2=params['yrange'][1]

x3=params['Xrange'][0]
x4=params['Xrange'][1]
y3=params['Yrange'][0]
y4=params['Yrange'][1]

#%% open the file, important always remember to close it
dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')

# 100 no charge no flexo
px_100 = np.array(dataAll['/burg_100/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
pz_100 = np.array(dataAll['/burg_100/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
pLength_100 = np.sqrt(np.square(np.array(px_100))+np.square(np.array(pz_100)))

# 100 with charge no flexo
px_100_q = np.array(dataAll['/burg_100_with_charge_no_flexo/'+params['datFold']+'/px'][x1:x2,y1:y2])
pz_100_q = np.array(dataAll['/burg_100_with_charge_no_flexo/'+params['datFold']+'/pz'][x1:x2,y1:y2])
pLength_100_q = np.sqrt(np.square(np.array(px_100_q))+np.square(np.array(pz_100_q)))

# 100 with charge with flexo
px_100_q_f = np.array(dataAll['/burg_100_with_charge/'+params['datFold']+'/px'][x1:x2,y1:y2])
pz_100_q_f = np.array(dataAll['/burg_100_with_charge/'+params['datFold']+'/pz'][x1:x2,y1:y2])
pLength_100_q_f = np.sqrt(np.square(np.array(px_100_q_f))+np.square(np.array(pz_100_q_f)))

#110 no charge no flexo
px_110 = np.array(dataAll['/burg_110/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/px'][x3:x4,y3:y4])
pz_110 = np.array(dataAll['/burg_110/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/pz'][x3:x4,y3:y4])
pLength_110 = np.sqrt(np.square(np.array(px_110))+np.square(np.array(pz_110)))

# 110 with charge no flexo
px_110_q = np.array(dataAll['/burg_110_with_charge_no_flexo/'+params['datFold']+'/px'][x3:x4,y3:y4])
pz_110_q = np.array(dataAll['/burg_110_with_charge_no_flexo/'+params['datFold']+'/pz'][x3:x4,y3:y4])
pLength_110_q = np.sqrt(np.square(np.array(px_110_q))+np.square(np.array(pz_110_q)))

# 110 with charge with flexo
px_110_q_f = np.array(dataAll['/burg_110_with_charge/'+params['datFold']+'/px'][x3:x4,y3:y4])
pz_110_q_f = np.array(dataAll['/burg_110_with_charge/'+params['datFold']+'/pz'][x3:x4,y3:y4])
pLength_110_q_f = np.sqrt(np.square(np.array(px_110_q_f))+np.square(np.array(pz_110_q_f)))


px_100_flexo = np.array(dataAll['/burg_100/pxBins_0.08_2.6_2.2'])
py_100_flexo = np.array(dataAll['/burg_100/pyBins_0.08_2.6_2.2'])
pz_100_flexo = np.array(dataAll['/burg_100/pzBins_0.08_2.6_2.2'])

px_110_flexo = np.array(dataAll['/burg_110/pxBins_0.08_2.6_2.2'])
py_110_flexo = np.array(dataAll['/burg_110/pyBins_0.08_2.6_2.2'])
pz_110_flexo = np.array(dataAll['/burg_110/pzBins_0.08_2.6_2.2'])

px_100_charge_flexo = np.array(dataAll['/burg_100_with_charge/pxBins_1.0'])
py_100_charge_flexo = np.array(dataAll['/burg_100_with_charge/pyBins_1.0'])
pz_100_charge_flexo = np.array(dataAll['/burg_100_with_charge/pzBins_1.0'])

px_110_charge_flexo = np.array(dataAll['/burg_110_with_charge/pxBins_1.0'])
py_110_charge_flexo = np.array(dataAll['/burg_110_with_charge/pyBins_1.0'])
pz_110_charge_flexo = np.array(dataAll['/burg_110_with_charge/pzBins_1.0'])

dataAll.close()

pRange=[min(pLength_100.min(),pLength_100_q.min(),pLength_100_q_f.min(),pLength_110.min(),pLength_110_q.min(),pLength_110_q_f.min()),max(pLength_100.max(),pLength_100_q.max(),pLength_100_q_f.max(),pLength_110.max(),pLength_110_q.max(),pLength_110_q_f.max())]



#%% sub figure a,b,c the polarization for f11,f12,f44 =1, range from [-5e10,5e10]
cmapGlob = 'viridis'


scale = pRange[1]*15

#scale =1e11
heatQuiverPlot_noEdge(pLength_100,px_100,pz_100,pRange,5,scale,params['imgDir']+'/figure_4c_p100_no_charge_no_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red')
heatQuiverPlot_noEdge(pLength_100_q,px_100_q,pz_100_q,pRange,5,scale,params['imgDir']+'/figure_4a_p100_with_charge_no_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red')
heatQuiverPlot_noEdge(pLength_100_q_f,px_100_q_f,pz_100_q_f,pRange,5,scale,params['imgDir']+'/figure_4b_p100_with_charge_with_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red')
plotColorBar(params['imgDir']+'/figure_4_p100_ColorBar.svg','Polarization (C/m^2)',[a for a in pRange],5.0,0.7,0.0,1.3,20,'%+.1f','',cmapGlob)

#%% sub figure c 





#%% sub figure d,e,f the stress gradient, range from []
cmapGlob = 'viridis'


scale = pRange[1]*15

#scale =1e11
heatQuiverPlot_noEdge(pLength_110,px_110,pz_110,pRange,5,scale,params['imgDir']+'/figure_4d_p110_no_charge_no_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45)
heatQuiverPlot_noEdge(pLength_110_q,px_110_q,pz_110_q,pRange,5,scale,params['imgDir']+'/figure_4e_p110_with_charge_no_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45)
heatQuiverPlot_noEdge(pLength_110_q_f,px_110_q_f,pz_110_q_f,pRange,5,scale,params['imgDir']+'/figure_4f_p110_with_charge_with_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45)
plotColorBar(params['imgDir']+'/figure_4_p110_ColorBar.svg','Polarization (C/m^2)',[a for a in pRange],5.0,0.7,0.0,1.3,20,'%+.1f','',cmapGlob)

#%%
texEquation(r'b=(100) +Qd, -flexo',2.5,0.4,params['imgDir']+'/b100_q.svg',15)
texEquation(r'b=(100) +Qd, +flexo',2.5,0.4,params['imgDir']+'/b100_q_f.svg',15)
texEquation(r'b=(110) -Qd, -flexo',2.5,0.4,params['imgDir']+'/b110.svg',15)
texEquation(r'b=(110) +Qd, -flexo',2.5,0.4,params['imgDir']+'/b110_q.svg',15)
texEquation(r'b=(110) +Qd, +flexo',2.5,0.4,params['imgDir']+'/b110_q_f.svg',15)
#%%



#%% Combine the individual figure files
#figures=[[params['ImgDir']+'figure_1a_stress11.svg',params['ImgDir']+'figure_1a_stress33.svg',params['ImgDir']+'figure_1a_stress13.svg',params['ImgDir']+'figure_1-stressColorBar.svg'],
#         [params['ImgDir']+'figure_1d_sGrad11.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1-stressGradientColorBar.svg']]
figureName=[['figure_4a_p100_with_charge_no_flexo.svg','figure_4b_p100_with_charge_with_flexo.svg','figure_4c_p100_no_charge_no_flexo.svg','figure_4_p110_ColorBar.svg'],
            ['b100_q.svg','b100_q_f.svg','',''],
            ['figure_4d_p110_no_charge_no_flexo.svg','figure_4e_p110_with_charge_no_flexo.svg','figure_4f_p110_with_charge_with_flexo.svg','figure_4_p110_ColorBar.svg'],
            ['b110.svg','b110_q.svg','b110_q_f.svg','']]


figureWidth = 190
figureHeight= 205
textSize = "20"
textBase = 20
textLeft = 5
boxSize="25"
offset=0
offsetY = 184
figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,4)] for i in range(0,4)]
sc.Figure('17cm','11cm',
          sc.Panel(
                  sc.SVG(figures[0][0]).scale(0.5),
                  sc.Element(etree.Element("rect",width=boxSize,height=boxSize,fill="rgb(255,255,255)")),
                  sc.Text('a',0+textLeft,textBase,size=textSize)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][1]).scale(0.5).move(figureWidth,0),
                  sc.Element(etree.Element("rect",width=boxSize,height=boxSize,fill="rgb(255,255,255)")).move(figureWidth,0),
                  sc.Text('b',figureWidth+textLeft,textBase,size=textSize)
                  ),
#          sc.Panel(
#                  sc.SVG(figures[0][2]).scale(180/265.0).move(figureWidth*2,0)
#                  ),
          sc.Panel(
                  sc.SVG(figures[0][2]).scale(0.5).move(figureWidth*2,0),
                  sc.Element(etree.Element("rect",width=boxSize,height=boxSize,fill="rgb(255,255,255)")).move(figureWidth*2,0),
                  sc.Text('c',figureWidth*2+textLeft,textBase,size=textSize)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][0]).move(offset+figureWidth*0,offsetY)
                  ),          
          sc.Panel(
                  sc.SVG(figures[1][1]).move(offset+figureWidth*1,offsetY)
                  ),
#          sc.Panel(
#                  sc.SVG(figures[1][2]).move(offset+figureWidth*2,180)
#                  ),
          sc.Panel(
                  sc.SVG(figures[2][0]).scale(0.5).move(0,figureHeight),
                  sc.Element(etree.Element("rect",width=boxSize,height=boxSize,fill="rgb(255,255,255)")).move(0,figureHeight),
                  sc.Text('d',0+textLeft,textBase+figureHeight,size=textSize)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][1]).scale(0.5).move(figureWidth,figureHeight),
                  sc.Element(etree.Element("rect",width=boxSize,height=boxSize,fill="rgb(255,255,255)")).move(figureWidth,figureHeight),
                  sc.Text('e',figureWidth+textLeft,textBase+figureHeight,size=textSize)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][2]).scale(0.5).move(figureWidth*2,figureHeight),
                  sc.Element(etree.Element("rect",width=boxSize,height=boxSize,fill="rgb(255,255,255)")).move(figureWidth*2,figureHeight),
                  sc.Text('f',figureWidth*2+textLeft,textBase+figureHeight,size=textSize)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][3]).scale(0.5).move(figureWidth*3,figureHeight)
                  ),
          sc.Panel(
                  sc.SVG(figures[3][0]).move(offset+figureWidth*0,offsetY+figureHeight)
                  ),          
          sc.Panel(
                  sc.SVG(figures[3][1]).move(offset+figureWidth*1,offsetY+figureHeight)
                  ),
          sc.Panel(
                  sc.SVG(figures[3][2]).move(offset+figureWidth*2,offsetY+figureHeight)
                  )
          ).save(params['imgFile'])
#          sc.Grid(20,20)
#%%
fixSVGASCII(params['imgFile'])