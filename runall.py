# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 12:18:38 2019

@author: cxx
"""

import figure_1
import figure_2
import figure_3
import figure_4
import sup_1
import sup_2
import sup_3
import sup_4
import sup_5
import sup_6
import sup_7
#import sup_8
#import sup_9

from STODislocationHeader import *

#%%

def generateVueTemplate(path='./',figureList=[]):
    path = path + 'website/'
    touch(path)
    touch(path+'.vuepress')
    touch(path+'.vuepress/public')
#    touchFile(path+'.vuepress/config.js')
#    touchFile(path+'README.md')
#    touchFile(path+'Figures.md')
#    touchFile(path+'package.json')]
#    touchFile(path+'.gitignore')
    
    json = """{
  "name": "website",
  "version": "1.0.0",
  "description": "paper website",
  "main": "index.js",
  "scripts": {
    "test": "echo 'Error: no test specified' && exit 1",
    "build": "vuepress build"
  },
  "repository": {
    "type": "git",
    "url": "git+ssh://git@gitlab.com/chengxiaoxing/sto-dislocation-flexo-site.git"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "vuepress": "0.14.11"
  },
  "bugs": {
    "url": "https://gitlab.com/chengxiaoxing/sto-dislocation-flexo-site/issues"
  },
  "homepage": "https://gitlab.com/chengxiaoxing/sto-dislocation-flexo-site#readme"
}

    """
    config = """module.exports = {
    themeConfig: {
        displayAllHeaders: true,
        nav:[
            {text: 'Home',link:'/'},
            {text: 'Figures',link:'Figures'}
        ],
    }
  }
    """
    main = """---
home: true
heroImage: /hero.jpeg
title: The papers title
lang: en-US
sidebar: auto
---
    """

    figure = """---
title: The papers title
lang: en-US
sidebar: auto
---
    """
    with open(path+".gitignore",'w') as file:
        file.write(".vuepress/dist\n");
        file.write("node_modules/");
        
    with open(path+"package.json",'w') as file:
        file.write(json);
        
    with open(path+".vuepress/config.js",'w') as file:
        file.write(config);
    
    with open(path+"README.md",'w') as file:
        file.write(main);

    with open(path+"Figures.md",'w') as file:
        file.write(figure);
        
    for figure in figureList:
        copy(figure+'.svg',path+'.vuepress/public/')
    
    for captionFile in figureList:
        with open(captionFile+'.md','r') as file:
            caption = file.read()
        
        with open(path+'Figures.md','a') as file:
            file.write(caption)
            
#%% generate the website 

figureList = ['figure1','figure2','figure3','figure4','sup1','sup2','sup3','sup4','sup5','sup6','sup7']
figureList = ["./img-new/"+a for a in figureList]
generateVueTemplate('./',figureList)

