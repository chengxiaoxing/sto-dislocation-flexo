# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 18:44:47 2019

@author: cxx
"""

import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *
#%%
params={}

params['imgDir']='./img-new/figure2'
params['imgFile']='./img-new/figure2.svg'

params['ImgDir']='\\img-new\\figure2\\'
params['datDir']='/burg_100'
params['curDir']="D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper"
params['start'] = 2000
params['polar'] = 'Polar.00002000.dat'
params['stress'] = 'Stress.00002001.dat'
params['flexo'] = 'Eflexo.00002001.dat'

params['nx']=512
params['ny']=1
params['nz']=512

params['xrange']=[244,268]
params['yrange']=[114,138]

params['disLocX']=0.5
params['disLocY']=0.5

params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']

touch(params['imgDir'])


params['captionFile'] = './img-new/figure2.md'
params['caption'] = """

## Figure 2
![Figure 2](figure2.svg)

Comparison of polarization distribution with and without flexoelectric effect.
(a) polarization with no polarization (case 1), 
(b) polarization with experiment flexoelectric coefficient (case 2),
The white quiver in (a) and (b) represent polarization vector, and the heat map in background shows the polarization magnitude.
(c) Px, Pz and P magnitude statistics for (a) and (b), each bar is the result of 5 calculations starting from different random noise, and the black line on top shows the standard deviation of the data. 
The upper panel is the summation of polarization's absolute value at each grid point in (a) and (b), the lower panel is the maximum polarization value.
(d) and (e) give the flexoelectric field distribution of the same region. The white quiver shows the flexoelectric field vector, which is the same for (d) and (e), and the background heat map is the x and z component of flexoelectric field.
(f) Experimental HRTEM result.

"""

#%%
#dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')
dataAll= h5py.File('20190422_STO_dislocation_flexo_mech.h5','r')


stressRange=[-5e10,5e10]
gradientRange=[0,5e10]
x1=params['xrange'][0]
x2=params['xrange'][1]
z1=params['yrange'][0]
z2=params['yrange'][1]

px_no_flexo = np.array(dataAll[params['datDir']+'/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/px'])[x1:x2,z1:z2]
pz_no_flexo = np.array(dataAll[params['datDir']+'/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/pz'])[x1:x2,z1:z2]
p_no_flexo_length = np.sqrt(np.square(px_no_flexo)+np.square(pz_no_flexo))
px_flexo = np.array(dataAll[params['datDir']+'/36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/px'])[x1:x2,z1:z2]
pz_flexo = np.array(dataAll[params['datDir']+'/36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/pz'])[x1:x2,z1:z2]
p_flexo_length = np.sqrt(np.square(px_flexo)+np.square(pz_flexo))

pRange=[min(p_no_flexo_length.min(),p_flexo_length.min()),max(p_no_flexo_length.max(),p_flexo_length.max())]
pscale = max(p_no_flexo_length.max(),p_flexo_length.max())*20


eflexoX = np.array(dataAll[params['datDir']+'/36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/flexoField1'])[x1:x2,z1:z2]
eflexoZ = np.array(dataAll[params['datDir']+'/36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/flexoField3'])[x1:x2,z1:z2]
eLength = np.sqrt(np.square(eflexoX)+np.square(eflexoZ))
flexoRange=max(abs(min(eflexoX.min(),eflexoZ.min())),abs(max(eflexoX.max(),eflexoZ.max())))
eRange = [-flexoRange,flexoRange]
escale = eLength.max()*20

px_max_flexo = dataAll['/burg_100'].attrs['pxmax_0.08_2.6_2.2']
py_max_flexo = dataAll['/burg_100'].attrs['pymax_0.08_2.6_2.2']
pz_max_flexo = dataAll['/burg_100'].attrs['pzmax_0.08_2.6_2.2']
p_max_flexo  = dataAll['/burg_100'].attrs['pmax_0.08_2.6_2.2']

px_avg_flexo = dataAll['/burg_100'].attrs['pxavg_0.08_2.6_2.2'] #*512*512
py_avg_flexo = dataAll['/burg_100'].attrs['pyavg_0.08_2.6_2.2'] #*512*512
pz_avg_flexo = dataAll['/burg_100'].attrs['pzavg_0.08_2.6_2.2'] #*512*512
p_avg_flexo  = dataAll['/burg_100'].attrs['pavg_0.08_2.6_2.2'] #*512*512

px_max_no_flexo = dataAll['/burg_100'].attrs['pxmax_0_0_0']
py_max_no_flexo = dataAll['/burg_100'].attrs['pymax_0_0_0']
pz_max_no_flexo = dataAll['/burg_100'].attrs['pzmax_0_0_0']
p_max_no_flexo  = dataAll['/burg_100'].attrs['pmax_0_0_0']

px_avg_no_flexo = dataAll['/burg_100'].attrs['pxavg_0_0_0'] #*512*512
py_avg_no_flexo = dataAll['/burg_100'].attrs['pyavg_0_0_0'] #*512*512
pz_avg_no_flexo = dataAll['/burg_100'].attrs['pzavg_0_0_0'] #*512*512
p_avg_no_flexo  = dataAll['/burg_100'].attrs['pavg_0_0_0'] #*512*512

dataAll.close()


#%% sub figure2 a,b

cmapGlob = 'viridis'

patch = getScaleBar(4,1,5,1.5,'2nm',color='white',lw=5,fs=30,padding=[1,1],bg = 'None')
heatQuiverPlot_noEdge(p_no_flexo_length,px_no_flexo,pz_no_flexo,pRange,5,pscale,params['imgDir']+'/figure_2a_p_no_flexo.svg',disColor='red',disx=params['disLocX'],disy=params['disLocY'],artists=patch)
heatQuiverPlot_noEdge(p_flexo_length,px_flexo,pz_flexo,pRange,5,pscale,filename=params['imgDir']+'/figure_2b_p_with_flexo.svg',disColor='red',disx=params['disLocX'],disy=params['disLocY']) #linePos=0.36
plotColorBar(params['imgDir']+'/figure_2_p_colorbar.svg','Polarization (C/m^2)',pRange,5.0,0.7,0.0,1.3,20,'%+.2f','',cmapGlob)

#%% sub figure 2 c
choiceZ=9
polarZ = np.arange(0,9.6,0.4)
pxLine = px_flexo[:,choiceZ]
pzLine = pz_flexo[:,choiceZ]
px_no_Line = px_no_flexo[:,choiceZ]
pz_no_Line = pz_no_flexo[:,choiceZ]
scatterPlot([polarZ,polarZ,polarZ,polarZ],[pxLine,px_no_Line,pzLine,pz_no_Line],params['imgDir']+'/figure_2c_p_lines.svg',['px w/ flexo','px w/o flexo','pz w/ flexo','pz w/o flexo'],['#440154','#6E509E','#3FA34D','#2FB47C'],['s','o','s','o'],5,7,(0.1,0.63))

 #%% sub figure 2 d,e

cmapGlob = 'magma'

#quiverPlot_noEdge(eflexoX,eflexoZ,'img/paper_flexo_field.svg',3,3e9)
heatQuiverPlot_noEdge(eflexoX,eflexoX,eflexoZ,eRange,5,escale,params['imgDir']+'/figure_2d_flexoField_X.svg',disColor='green',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
heatQuiverPlot_noEdge(eflexoZ,eflexoX,eflexoZ,eRange,5,escale,params['imgDir']+'/figure_2e_flexoField_Y.svg',disColor='green',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob)
plotColorBar(params['imgDir']+'/figure_2_flexo_colorbar.svg','Flexo-field (V/m)',[ a/1.0e8 for a in eRange],4.6,0.7,0.4,1.3,20,'%+.2f','1e8',cmapGlob)

#%% sub figure 2, f
fig = plt.figure(figsize=(7,5))
names = ['Px','Pz','P total']
indices = np.array([1,2,3])
width = 0.34

ax1=fig.add_axes([0.0,0.0,0.85,0.5])
ax1.set_ylabel(r'$max(|p|)(C/m^2)$',fontsize=20)
ax1.bar(indices-width/2,[np.average(px_max_flexo),np.average(pz_max_flexo),np.average(p_max_flexo)],width=width,label = 'With flexo',color='#2FB47C',yerr=[np.std(px_max_flexo),np.std(py_max_flexo),np.std(pz_max_flexo)])
ax1.bar(indices+width/2,[np.average(px_max_no_flexo),np.average(pz_max_no_flexo),np.average(p_max_no_flexo)],width=width,label='Without flexo',color='#6E509E',yerr=[np.std(px_max_no_flexo),np.std(py_max_no_flexo),np.std(pz_max_no_flexo)])
ax1.set_xticks(indices)
ax1.set_yticks([0,0.05,0.1,0.15])
ax1.tick_params(bottom=False)
ax1.tick_params(labelsize=20)
ax1.set_xticklabels(names,fontsize=30)
ax1.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
ax1.yaxis.tick_right()
ax1.yaxis.set_label_position('right')
#ax1.legend(loc=9)


ax2=fig.add_axes([0.0,0.5,0.85,0.5],xticklabels=[])
ax2.set_ylabel(r'$avg(|p|)(10^{-3}C/m^2)$',fontsize=20)
scale=1e3
ax2.bar(indices-width/2,[np.average(px_avg_flexo*scale),np.average(pz_avg_flexo*scale),np.average(p_avg_flexo*scale)],width=width,label = 'With flexo',color='#2FB47C',yerr=[np.std(px_avg_flexo*scale),np.std(py_avg_flexo*scale),np.std(pz_avg_flexo*scale)])
ax2.bar(indices+width/2,[np.average(px_avg_no_flexo*scale),np.average(pz_avg_no_flexo*scale),np.average(p_avg_no_flexo*scale)],width=width,label='Without flexo',color='#6E509E',yerr=[np.std(px_avg_no_flexo*scale),np.std(py_avg_no_flexo*scale),np.std(pz_avg_no_flexo*scale)])
ax2.tick_params(bottom=False)
ax2.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
ax2.yaxis.tick_right()
ax2.tick_params(labelsize=20)
ax2.yaxis.set_label_position('right')
ax2.legend(loc=(0.2,0.6),fontsize=20)

plt.tight_layout()
plt.savefig(params['imgDir']+'/figure_2f_p_max_avg_stat.svg',dpi=300)
plt.show()
plt.close()

#%%

texEquation(r'P, no flexo',2.5,0.4,params['imgDir']+'/p_without_flexo.svg',15)
texEquation(r'P, exp. flexo',2.5,0.4,params['imgDir']+'/p_with_flexo.svg',15)
texEquation(r'Flexo field x',2.5,0.4,params['imgDir']+'/eflexoX.svg',15)
texEquation(r'Flexo field z',2.5,0.4,params['imgDir']+'/eflexoY.svg',15)


#%% combine the figures together

#figureName=[['figure_2a_p_no_flexo.svg','figure_2b_p_with_flexo.svg','figure_2_p_colorbar.svg','figure_2c_p_lines.svg'],
#            ['p_without_flexo.svg','p_with_flexo.svg','',''],
#            ['figure_2d_flexoField_X.svg','figure_2e_flexoField_Y.svg','figure_2_flexo_colorbar.svg','figure_2f_p_max_avg_stat.svg'],
#            ['eflexoX.svg','eflexoY.svg','','']]

figureName=[['figure_2a_p_no_flexo.svg','figure_2b_p_with_flexo.svg','figure_2_p_colorbar.svg','figure_2f_p_max_avg_stat.svg'],
            ['p_without_flexo.svg','p_with_flexo.svg','',''],
            ['figure_2d_flexoField_X.svg','figure_2e_flexoField_Y.svg','figure_2_flexo_colorbar.svg',''],
            ['eflexoX.svg','eflexoY.svg','','']]

label = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['a','b','c','d','e','f']]
labelTransparent = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['aa','bb','cc','dd','ee','ff']]

figureWidth = 190
figureHeight = 205
textSize = "20"
textBase = 18
textLeft = 5
boxSize="25"
offsetX=0
offsetY=184
labelFont = 'DejaVu Sans'
labelWeight = 'normal'
figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,4)] for i in range(0,4)]
sc.Figure('19.5cm','11cm',
          sc.Panel(
                  sc.SVG(figures[0][0]).scale(0.5),
                  sc.SVG(label[0]).move(-1,-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][1]).scale(0.5).move(figureWidth,0),
                  sc.SVG(label[1]).move(figureWidth-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][2]).scale(0.5).move(figureWidth*2,0),
                  ),
          sc.Panel(
                  sc.SVG(figures[0][3]).scale(0.5).move(figureWidth*2+82,0),
                  sc.SVG(labelTransparent[2]).move(figureWidth*2-1+80,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][0]).move(offsetX+figureWidth*0,offsetY)
                  ),          
          sc.Panel(
                  sc.SVG(figures[1][1]).move(offsetX+figureWidth*1,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][0]).scale(0.5).move(0,figureHeight),
                  sc.SVG(label[3]).move(0-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][1]).scale(0.5).move(figureWidth,figureHeight),
                  sc.SVG(label[4]).move(figureWidth-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][2]).scale(0.5).move(figureWidth*2,figureHeight),
                  ),
#          sc.Panel(
#                  sc.SVG(figures[2][3]).scale(0.5).move(figureWidth*2+82,figureWidth+15),
#                  sc.SVG(labelTransparent[5]).move(figureWidth*2-1+80,figureHeight-1)
#                  ),
          sc.Panel(
                  sc.SVG(figures[3][0]).move(offsetX+figureWidth*0,offsetY+figureHeight)
                  ),          
          sc.Panel(
                  sc.SVG(figures[3][1]).move(offsetX+figureWidth*1,offsetY+figureHeight)
                  )
          ).save(params['imgFile'])
#%%
fixSVGASCII(params['imgFile'])
with open(params['captionFile'],'w') as file:
    file.write(params['caption'])