# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 21:15:22 2019

@author: cxx
"""
import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *

#%%
#def scatterPlot(dataX,dataY,filename,label,color,marker,height,width,location):
#    fig = plt.figure(figsize=(width,height))
#    ax=fig.gca()
#    for i  in range(len(label)):
#        ax.plot(dataX[i],dataY[i],label=label[i],color=color[i],marker=marker[i])
#    plt.legend(loc=location)
#    plt.tight_layout()
#    plt.savefig(filename,dpi=300)
#    plt.show()
#    plt.close()
    
def rotationMatrix(theta):
    rot = np.empty((3,3))
    rot[0,0] = cos(theta)*cos(theta)
    rot[0,1] = sin(theta)*sin(theta)
    rot[0,2] = 2*cos(theta)*sin(theta)
    rot[1,0] = sin(theta)*sin(theta)
    rot[1,1] = cos(theta)*cos(theta)
    rot[1,2] = -2*sin(theta)*cos(theta)
    rot[2,0] = -sin(theta)*cos(theta)
    rot[2,1] = sin(theta)*cos(theta)
    rot[2,2] = cos(theta)*cos(theta) - sin(theta)*sin(theta)
    return rot

def rotate(vector,theta):
    vector = np.array(vector)
    outVect = np.empty(3)
    rot = rotationMatrix(theta)
    print(vector)
    print(theta)
    outVect[0] = sum(rot[0]*vector)
    outVect[1] = sum(rot[1]*vector)
    outVect[2] = sum(rot[2]*vector)
    return outVect
#%%
params={}
params['imgDir']='./img-new/sup7'
params['imgFile']='./img-new/sup7.svg'
params['ImgDir']='\\img-new\\sup7\\'

touch(params['imgDir'])
params['xrange']=[244,268]
params['yrange']=[114,138]

x1=params['xrange'][0]
x2=params['xrange'][1]
z1=params['yrange'][0]
z2=params['yrange'][1]
#%%
cmapGlob='inferno'
G=1.27e11
v=0.2415
b=0.4e-9
pi=3.141592653589
coeff=G*b/(2*pi*(1-v))
#%%
#x=np.linspace(-9.5,9.5,20)*1e-9
#y=np.linspace(-9.5,9.5,20)*1e-9
x=np.linspace(-5.0,5.0,10)*1e-9
y=np.linspace(-5.0,5.0,10)*1e-9
xv,yv=np.meshgrid(x,y,indexing='ij')
div=np.square(np.square(xv)+np.square(yv))
anaS11=-coeff*yv*(3*xv*xv+yv*yv)/div
anaS33=coeff*yv*(xv*xv-yv*yv)/div
anaS13=coeff*xv*(xv*xv-yv*yv)/div
#anaS11 = np.swapaxes(anaS11,0,1)
#anaS33 = np.swapaxes(anaS33,0,1)
#anaS13 = np.swapaxes(anaS13,0,1)
anaSRange=[min(anaS11.min(),anaS33.min(),anaS13.min()),max(anaS11.max(),anaS33.max(),anaS13.max())]
#anaSRange=[]
#def heatPlot_noEdge(dataset,valRange=[],height=4,filename='',disx=0.5,disy=0.5,disColor='green',cmapGlob = 'viridis',linePos=0,disAngle=0):

heatPlot_noEdge(anaS11,anaSRange,5,params['imgDir']+'/figure_sup7_anaS11.svg',disColor='green',cmapGlob=cmapGlob)
heatPlot_noEdge(anaS33,anaSRange,5,params['imgDir']+'/figure_sup7_anaS33.svg',disColor='green',cmapGlob=cmapGlob)
heatPlot_noEdge(anaS13,anaSRange,5,params['imgDir']+'/figure_sup7_anaS13.svg',disColor='green',cmapGlob=cmapGlob)
anaSRange=[min(anaS11.min(),anaS33.min(),anaS13.min()),max(anaS11.max(),anaS33.max(),anaS13.max())]

anaSRange=[1e10,-1.5e10]
plotColorBar(params['imgDir']+'/figure_sup7_anaColorBar.svg','Analytical Stress (Pa)',[a/1e10 for a in anaSRange],4.6,0.7,0.4,1.3,20,'%+0.1f','1e10',cmapGlob=cmapGlob)

#%%
#x1=224;x2=289
#z1=96;z2=161
cmapGlob='inferno'
#dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')
dataAll= h5py.File('20190422_STO_dislocation_flexo_mech.h5','r')

stress11 = np.array(dataAll['/burg_110/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/stress11'])
stress33 = np.array(dataAll['/burg_110/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/stress33'])
stress13 = np.array(dataAll['/burg_110/01+&MATERIAL.FLEXOCON_0_0_0+&PNOISEED_1+QNOISEED_1/stress13'])

simS11 = np.swapaxes(stress11[x1:x2,z1:z2],0,1)
simS33 = np.swapaxes(stress33[x1:x2,z1:z2],0,1)
simS13 = np.swapaxes(stress13[x1:x2,z1:z2],0,1)
dataAll.close()

#%%

x=np.linspace(-5,5,11)*1e-9
y=np.linspace(-5,5,11)*1e-9
#x=np.linspace(-9.5,9.5,19)*1e-9
#y=np.linspace(-9.5,9.5,19)*1e-9
xv,yv=np.meshgrid(x,y,indexing='ij')
div=np.square(np.square(xv)+np.square(yv))

anaS11=-coeff*yv*(3*xv*xv+yv*yv)/div
anaS33=coeff*yv*(xv*xv-yv*yv)/div
anaS13=coeff*xv*(xv*xv-yv*yv)/div
anaS11 = np.swapaxes(anaS11,0,1)
anaS33 = np.swapaxes(anaS33,0,1)
anaS13 = np.swapaxes(anaS13,0,1)


anaSX = np.arange(-5,6,1)
simSX = np.arange(-4.6,5.0,0.4)

#anaSX = np.arange(0,19,1)
#simSX = np.arange(0,9.6*2,0.4*2)

anaX=6
simX=14

#anaX=10
#simX=12

anaS11Line = anaS11[:,anaX]
#anaSX = np.arange(0,20,2)
simS11Line = simS11[:,simX]
#simSX = np.arange(0,20,0.5)

#anaSimRange = [1.2*min(anaS11Line.min(),simS11Line.min(),
#                   anaS13Line.min(),simS13Line.min(),
#                   anaS33Line.min(),simS33Line.min()),
#               1.2*max(anaS11Line.max(),simS11Line.max(),
#                   anaS13Line.max(),simS13Line.max(),
#                   anaS33Line.max(),simS33Line.max())]
               
anaSimRange11 = [1.2*min(anaS11Line.min(),simS11Line.min()),
               1.2*max(anaS11Line.max(),simS11Line.max())]



scatterPlot([anaSX,simSX],[anaS11Line,simS11Line],params['imgDir']+'/figure_sup7_ana_simS11.svg',['Analytical','Simulation'],['b','r'],['o','o'],5,5.5,1,yrange=anaSimRange11)


anaS33Line = anaS33[:,anaX]
simS33Line = simS33[:,simX]

anaSimRange33 = [1.2*min(anaS33Line.min(),simS33Line.min()),
               1.2*max(anaS33Line.max(),simS33Line.max())]
scatterPlot([anaSX,simSX],[anaS33Line,simS33Line],params['imgDir']+'/figure_sup7_ana_simS33.svg',['Analytical','Simulation'],['b','r'],['o','o'],5,5.5,1,yrange=anaSimRange33)

anaS13Line = anaS13[:,anaX]
simS13Line = simS13[:,simX]
anaSimRange13 = [1.2*min(anaS13Line.min(),simS13Line.min()),
               1.2*max(anaS13Line.max(),simS13Line.max())]

scatterPlot([anaSX,simSX],[anaS13Line,simS13Line],params['imgDir']+'/figure_sup7_ana_simS13.svg',['Analytical','Simulation'],['b','r'],['o','o'],5,5.5,1,yrange=anaSimRange13)

#%%

texEquation(r'$\sigma_{11}$',2.5,0.4,params['imgDir']+'/s11.svg',15)
texEquation(r'$\sigma_{13}$',2.5,0.4,params['imgDir']+'/s13.svg',15)
texEquation(r'$\sigma_{33}$',2.5,0.4,params['imgDir']+'/s33.svg',15)

#%%
figureName=[['figure_sup7_anaS11.svg','figure_sup7_anaS13.svg','figure_sup7_anaS33.svg','figure_sup7_anaColorBar.svg'],
            ['s11.svg','s13.svg','s33.svg',''],
            ['figure_sup7_ana_simS11.svg','figure_sup7_ana_simS13.svg','figure_sup7_ana_simS33.svg',''],
            ['','','','']]

label = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['a','b','c','d','e','f']]
labelTransparent = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['aa','bb','cc','dd','ee','ff']]

figureWidth = 190
figureHeight= 205
textSize = "20"
textBase = 18
textLeft = 5
boxSize="25"
offset=0
offsetY = 184
labelFont = 'DejaVu Sans'
labelWeight = 'normal'
figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,4)] for i in range(0,3)]
sc.Figure('17cm','11cm',
          sc.Panel(
                  sc.SVG(figures[0][0]).scale(0.5),
                  sc.SVG(label[0]).move(-1,-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][1]).scale(0.5).move(figureWidth,0),
                  sc.SVG(label[1]).move(figureWidth-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][2]).scale(0.5).move(figureWidth*2,0),
                  sc.SVG(label[2]).move(figureWidth*2-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][3]).scale(0.5).move(figureWidth*3,0),
#                  sc.Element(etree.Element("rect",width=boxSize,height=boxSize,fill="rgb(255,255,255)")).move(figureWidth*3,0),
#                  sc.Text('d',figureWidth*3+textLeft,textBase,size=textSize)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][0]).move(offset+figureWidth*0,offsetY)
                  ),          
          sc.Panel(
                  sc.SVG(figures[1][1]).move(offset+figureWidth*1,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][2]).move(offset+figureWidth*2,offsetY)
                  ),
#          sc.Panel(
#                  sc.SVG(figures[1][3]).move(offset+figureWidth*3,offsetY)
#                  ),
          sc.Panel(
                  sc.SVG(figures[2][0]).scale(0.5).move(0,figureHeight),
                  sc.SVG(labelTransparent[3]).move(0-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][1]).scale(0.5).move(figureWidth*1+10,figureHeight),
                  sc.SVG(labelTransparent[4]).move(figureWidth+10-1,figureHeight-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[2][2]).scale(0.5).move(figureWidth*2+20,figureHeight),
                  sc.SVG(labelTransparent[5]).move(figureWidth*2+20-1,figureHeight-1)
                  )
#          sc.Panel(
#                  sc.SVG(figures[2][3]).scale(0.5).move(figureWidth*3,figureHeight)
#                  )
#          sc.Panel(
#                  sc.SVG(figures[3][0]).move(offset+figureWidth*0,offsetY+figureHeight)
#                  ),     
#          sc.Panel(
#                  sc.SVG(figures[3][1]).move(offset+figureWidth*1,offsetY+figureHeight)
#                  ),
#          sc.Panel(
#                  sc.SVG(figures[3][2]).move(offset+figureWidth*2,offsetY+figureHeight)
#                  )
          ).save(params['imgFile'])
#          sc.Grid(20,20)
#%%
fixSVGASCII(params['imgFile'])