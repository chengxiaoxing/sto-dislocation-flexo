# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 14:17:47 2019

@author: cxx
"""
import importlib
import sys
import os
import platform

if platform.system() == 'Windows':
    os.chdir("D:/SynologyDrive/Research/STO-Dislocation-Flexo/paper")
    newPath = os.getcwd()+'\\code'
elif platform.system() == 'Linux':
    os.chdir('/gpfs/scratch/xuc116/sto-dislocation')
    newPath = os.getcwd()+'/code'
else:
    os.chdir('/Users/xiaoxingcheng/SynologyDrive/Research/STO-Dislocation-Flexo/paper')    
    newPath = os.getcwd()+'/code'

if newPath not in sys.path:
    print("Append new path")
    sys.path.append(newPath)
    
from STODislocationHeader import *
#%%
params={}

params['imgDir']='./img-new/sup7'
params['imgFile']='./img-new/sup7.svg'

params['ImgDir']='\\img-new\\sup7\\'
params['datDir']='/burg_110/'
params['start'] = 2000

params['nx']=512
params['ny']=1
params['nz']=512

params['xrange']=[115,139]
params['yrange']=[115,139]

params['disLocX']=0.49
params['disLocY']=0.49

params['batchKey']=['&MATERIAL.FLEXOCON','&PNOISEED','&QNOISEED']

touch(params['imgDir'])

x1=params['xrange'][0]
x2=params['xrange'][1]
y1=params['yrange'][0]
y2=params['yrange'][1]

params['captionFile'] = './img-new/sup7.md'
params['caption'] = """

## Supplementary 7
![Supplementary 7](sup7.svg)

The flexoelectric field distribution with different flexoelectric coefficients (case 2, 3, 4, and 5) for (110) dislocation.
(a), (b), (c)) and (d) flexoelectric field distribution for case 2, 3, 4, and 5 respectively, the white quiver shows the field vector, and background heat maps represent the flexoelectric field magnitude. 
The arrows in (b) is enlarged 4 times and 2 times in (c).
"""

#%% open the file, important always remember to close it
#dataAll= h5py.File('20190325_STO_dislocation_flexo.h5','r')
dataAll= h5py.File('20190422_STO_dislocation_flexo_mech.h5','r')

## f11=1
#px_100 = np.array(dataAll[params['datDir']+'06+&MATERIAL.FLEXOCON_1_0_0+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
#pz_100 = np.array(dataAll[params['datDir']+'06+&MATERIAL.FLEXOCON_1_0_0+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
#pLength_100 = np.sqrt(np.square(np.array(px_100))+np.square(np.array(pz_100)))


flexoX_exp = np.array(dataAll[params['datDir']+'36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/flexoField1'][x1:x2,y1:y2])
flexoZ_exp = np.array(dataAll[params['datDir']+'36+&MATERIAL.FLEXOCON_0.08_2.6_2.2+&PNOISEED_1+QNOISEED_1/flexoField3'][x1:x2,y1:y2])
flexoLength_exp = np.sqrt(np.square(np.array(flexoX_exp))+np.square(np.array(flexoZ_exp)))


flexoX_100 = np.array(dataAll[params['datDir']+'06+&MATERIAL.FLEXOCON_1_0_0+&PNOISEED_1+QNOISEED_1/flexoField1'][x1:x2,y1:y2])
flexoZ_100 = np.array(dataAll[params['datDir']+'06+&MATERIAL.FLEXOCON_1_0_0+&PNOISEED_1+QNOISEED_1/flexoField3'][x1:x2,y1:y2])
flexoLength_100 = np.sqrt(np.square(np.array(flexoX_100))+np.square(np.array(flexoZ_100)))

## f12=1
#px_010 = np.array(dataAll[params['datDir']+'21+&MATERIAL.FLEXOCON_0_2_0+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
#pz_010 = np.array(dataAll[params['datDir']+'21+&MATERIAL.FLEXOCON_0_2_0+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
#pLength_010 = np.sqrt(np.square(np.array(px_010))+np.square(np.array(pz_010)))
flexoX_010 = np.array(dataAll[params['datDir']+'16+&MATERIAL.FLEXOCON_0_1_0+&PNOISEED_1+QNOISEED_1/flexoField1'][x1:x2,y1:y2])
flexoZ_010 = np.array(dataAll[params['datDir']+'16+&MATERIAL.FLEXOCON_0_1_0+&PNOISEED_1+QNOISEED_1/flexoField3'][x1:x2,y1:y2])
flexoLength_010 = np.sqrt(np.square(np.array(flexoX_010))+np.square(np.array(flexoZ_010)))

## f44=1
#px_001 = np.array(dataAll[params['datDir']+'31+&MATERIAL.FLEXOCON_0_0_1+&PNOISEED_1+QNOISEED_1/px'][x1:x2,y1:y2])
#pz_001 = np.array(dataAll[params['datDir']+'31+&MATERIAL.FLEXOCON_0_0_1+&PNOISEED_1+QNOISEED_1/pz'][x1:x2,y1:y2])
#pLength_001 = np.sqrt(np.square(np.array(px_001))+np.square(np.array(pz_001)))
flexoX_001 = np.array(dataAll[params['datDir']+'26+&MATERIAL.FLEXOCON_0_0_1+&PNOISEED_1+QNOISEED_1/flexoField1'][x1:x2,y1:y2])
flexoZ_001 = np.array(dataAll[params['datDir']+'26+&MATERIAL.FLEXOCON_0_0_1+&PNOISEED_1+QNOISEED_1/flexoField3'][x1:x2,y1:y2])
flexoLength_001 = np.sqrt(np.square(np.array(flexoX_001))+np.square(np.array(flexoZ_001)))


dataAll.close()

#pRange=[min(pLength_100.min(),pLength_010.min(),pLength_001.min()),max(pLength_100.max(),pLength_010.max(),pLength_001.max())]
flexoRange = [min(flexoLength_exp.min(),flexoLength_100.min(),flexoLength_010.min(),flexoLength_001.min()),max(flexoLength_exp.max(),flexoLength_100.max(),flexoLength_010.max(),flexoLength_001.max())]


#%% sub figure a,b,c the polarization for f11,f12,f44 =1, range from [-5e10,5e10]
#cmapGlob = 'viridis'
#
#
#scale = pRange[1]*15
#
##scale =1e11
#patch = getScaleBar(4,1,5,1.5,'2nm',color='white',lw=5,fs=30,padding=[1,1],bg = 'None')
#heatQuiverPlot_noEdge(pLength_100,px_100,pz_100,pRange,5,scale,params['imgDir']+'/figure_sup5a_f11=1_p.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45,artists = patch)
#heatQuiverPlot_noEdge(pLength_010,px_010,pz_010,pRange,5,scale,params['imgDir']+'/figure_sup5b_f12=1_p.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45)
#heatQuiverPlot_noEdge(pLength_001,px_001,pz_001,pRange,5,scale,params['imgDir']+'/figure_sup5c_f44=1_p.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disColor='red',disAngle=45)
#plotColorBar(params['imgDir']+'/figure_sup5_p_ColorBar.svg','Polarization (C/m^2)',[a for a in pRange],5.0,0.7,0.0,1.3,20,'%+.2f','',cmapGlob)

#%% sub figure d,e,f the stress gradient, range from []
cmapGlob = 'inferno'


scale = flexoRange[1]*15

#scale =1e11
patch = getScaleBar(4,1,5,1.5,'2nm',color='white',lw=5,fs=30,padding=[1,1],bg = 'None')
heatQuiverPlot_noEdge(flexoLength_exp,flexoX_exp,flexoZ_exp,flexoRange,5,scale,params['imgDir']+'/figure_sup7a_exp_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disAngle=45,artists=patch)

times = 4
patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
heatQuiverPlot_noEdge(flexoLength_100,flexoX_100,flexoZ_100,flexoRange,5,scale/times,params['imgDir']+'/figure_sup7b_f11=1_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disAngle=45,artists=patch)

times = 2
patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
heatQuiverPlot_noEdge(flexoLength_010,flexoX_010,flexoZ_010,flexoRange,5,scale/times,params['imgDir']+'/figure_sup7c_f12=1_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disAngle=45,artists=patch)

times = 1
patch = [ Text(23, 23,'x'+str(times),va='top',ha='right',zorder=99,color='white',fontsize=30)]
patch=[]
heatQuiverPlot_noEdge(flexoLength_001,flexoX_001,flexoZ_001,flexoRange,5,scale/times,params['imgDir']+'/figure_sup7d_f44=1_flexo.svg',disx=params['disLocX'],disy=params['disLocY'],cmapGlob=cmapGlob,disAngle=45,artists=patch)
plotColorBar(params['imgDir']+'/figure_sup7-flexo_ColorBar.svg','Flexo field (V/m)',[a/1.0e8 for a in flexoRange],4.6,0.7,0.4,1.3,20,'%+.1f','1e8',cmapGlob)

#%%

texEquation(r'exp. coeff., $\mathrm{flexo\; E}$',2.5,0.4,params['imgDir']+'/exp_flexo.svg',15)
texEquation(r'$V_{1111}=1V,\; \mathrm{flexo\; E}$',2.5,0.4,params['imgDir']+'/f11_flexo.svg',15)
texEquation(r'$V_{1122}=1V,\; \mathrm{flexo\; E}$',2.5,0.4,params['imgDir']+'/f12_flexo.svg',15)
texEquation(r'$V_{1212}=1V,\; \mathrm{flexo\; E}$',2.5,0.4,params['imgDir']+'/f44_flexo.svg',15)
#%%



#%% Combine the individual figure files
#figures=[[params['ImgDir']+'figure_1a_stress11.svg',params['ImgDir']+'figure_1a_stress33.svg',params['ImgDir']+'figure_1a_stress13.svg',params['ImgDir']+'figure_1-stressColorBar.svg'],
#         [params['ImgDir']+'figure_1d_sGrad11.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1d_sGrad33.svg',params['ImgDir']+'figure_1-stressGradientColorBar.svg']]
figureName=[['figure_sup7a_exp_flexo.svg','figure_sup7b_f11=1_flexo.svg','figure_sup7c_f12=1_flexo.svg','figure_sup7d_f44=1_flexo.svg','figure_sup7-flexo_ColorBar.svg'],
            ['exp_flexo.svg','f11_flexo.svg','f12_flexo.svg','f44_flexo.svg','']]

label = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['a','b','c','d','e','f']]
labelTransparent = [os.getcwd()+'\\img-new\\'+ name+'.svg' for name in ['aa','bb','cc','dd','ee','ff']]

figureWidth = 190
figureHeight = 205
textSize = "20"
textBase = 18
textLeft = 5
boxSize="25"
offset=0
offsetY = 184
labelFont = 'DejaVu Sans'
labelWeight = 'normal'
figures = [[os.getcwd()+params['ImgDir']+figureName[i][j] for j in range(0,5)] for i in range(0,2)]
sc.Figure('22cm','5.5cm',
          sc.Panel(
                  sc.SVG(figures[0][0]).scale(0.5),
                  sc.SVG(label[0]).move(-1,-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][1]).scale(0.5).move(figureWidth,0),
                  sc.SVG(label[1]).move(figureWidth-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][2]).scale(0.5).move(figureWidth*2,0),
                  sc.SVG(label[2]).move(figureWidth*2-1,0-1)
                  ),
          sc.Panel(
                  sc.SVG(figures[0][3]).scale(0.5).move(figureWidth*3,0),
                  sc.SVG(label[3]).move(figureWidth*3-1,0-1)
                  ),          
          sc.Panel(
                  sc.SVG(figures[0][4]).scale(0.5).move(figureWidth*4,0)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][0]).move(offset+figureWidth*0,offsetY)
                  ),          
          sc.Panel(
                  sc.SVG(figures[1][1]).move(offset+figureWidth*1,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][2]).move(offset+figureWidth*2,offsetY)
                  ),
          sc.Panel(
                  sc.SVG(figures[1][3]).move(offset+figureWidth*3,offsetY)
                  )
          ).save(params['imgFile'])
#          sc.Grid(20,20)
#%%
fixSVGASCII(params['imgFile'])          
with open(params['captionFile'],'w') as file:
    file.write(params['caption'])

